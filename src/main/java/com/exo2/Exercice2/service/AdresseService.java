package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.AdresseDto;
import com.exo2.Exercice2.entity.Adresse;
import com.exo2.Exercice2.mapper.AdresseMapper;
import com.exo2.Exercice2.repository.AdresseRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdresseService {
    private final AdresseRepository adresseRepository;
    private final AdresseMapper adresseMapper;

    public List<AdresseDto> findAll(Pageable pageable) {
        return adresseRepository.findAll(pageable).map(adresseMapper::toDto).getContent();
    }

    public AdresseDto findById(Long id) {
        return adresseMapper.toDto(adresseRepository.findById(id).orElse(null));
    }

    public Page<AdresseDto> findByVille(String ville, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Adresse> adressePage = adresseRepository.findAdresseByVille(ville, pageable);
        return adressePage.map(adresseMapper::toDto);
    }
}
